package ru.gb.simplenotes;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements ListFragment.Contract, EditFragment.Contract {

    private static final String NOTES_LIST_FRAGMENT_TAG = "NOTES_LIST_FRAGMENT_TAG";

    private boolean isTwoPaneMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isTwoPaneMode = findViewById(R.id.fragment_container_optional) != null;
        showList();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.searchbar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Search Notes");

        SearchView.OnQueryTextListener textListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        };
        searchView.setOnQueryTextListener(textListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.delete_all_notes) {
            Toast.makeText(this, "Delete all notes", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showList() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container_main, new ListFragment(), NOTES_LIST_FRAGMENT_TAG)
                .commit();
    }

    private void showEdit() {
        showEdit(null);
    }

    private void showEdit(@Nullable Notes note) {
        if (!isTwoPaneMode) {
            setTitle(note == null ? R.string.create_note_title : R.string.edit_note_title);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (!isTwoPaneMode) {
            transaction.addToBackStack(null);
        }
        transaction.replace(isTwoPaneMode ? R.id.fragment_container_optional : R.id.fragment_container_main
                , EditFragment.newInstance(note))
                .commit();
    }

    @Override
    public void createNewNote() {
        showEdit();
    }

    @Override
    public void editNote(Notes note) {
        showEdit(note);
    }

    @Override
    public void saveNote(Notes note) {
        setTitle(R.string.app_name);
        getSupportFragmentManager().popBackStack();
        ListFragment listFragment = (ListFragment) getSupportFragmentManager().findFragmentByTag(NOTES_LIST_FRAGMENT_TAG);
        listFragment.addNote(note);
    }
}