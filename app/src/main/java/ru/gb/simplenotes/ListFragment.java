package ru.gb.simplenotes;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    //private static final String PREF_NAME = "notes";
    private final ArrayList<Notes> noteList = new ArrayList<>();
    private FloatingActionButton createButton;
    private RecyclerView recyclerViewList;
    private NotesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        createButton = view.findViewById(R.id.button_create_note);
        recyclerViewList = view.findViewById(R.id.recycler_view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapter = new NotesAdapter(this::onItemDeleteClickListener);
        adapter.setOnItemClickListener(getContract()::editNote);
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.setAdapter(adapter);
        renderList(noteList);
        createButton.setOnClickListener(v -> getContract().createNewNote());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof Contract)) {
            throw new IllegalStateException("Activity must implements Contract");
        }
    }

    public void addNote(Notes newNote) {
        Notes sameNote = findNoteWithId(newNote.id);
        if (sameNote != null) {
            noteList.remove(sameNote);
        }
        noteList.add(newNote);
        renderList(noteList);
    }

    public void onItemDeleteClickListener(Notes note) {
        Notes deleteNote = findNoteWithId(note.id);
        noteList.remove(deleteNote);
        renderList(noteList);
    }

    @Nullable
    private Notes findNoteWithId(String id) {
        for (Notes note : noteList) {
            if (note.id.equals(id)) {
                return note;
            }
        }
        return null;
    }

    public void renderList(List<Notes> notes) {
        adapter.setData(notes);
    }


    private Contract getContract() {
        return (Contract) getActivity();
    }

    interface Contract {
        void createNewNote();

        void editNote(Notes note);
    }
}
