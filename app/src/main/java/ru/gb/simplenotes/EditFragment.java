package ru.gb.simplenotes;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EditFragment extends Fragment {

    private static final String NOTE_EXTRA_KEY = "NOTE_EXTRA_KEY";

    //private Button buttonSave;
    private FloatingActionButton buttonSave;
    private EditText editTextTitle;
    private EditText editTextDescription;

    @Nullable
    private Notes note = null;

    public static EditFragment newInstance(@Nullable Notes note) {
        EditFragment fragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(NOTE_EXTRA_KEY, note);
        fragment.setArguments(bundle);
        return fragment;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgament_edit, container, false);
        buttonSave = view.findViewById(R.id.button_save);
        editTextTitle = view.findViewById(R.id.title_edit_text);
        editTextDescription = view.findViewById(R.id.description_edit_text);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        note = (Notes) getArguments().getSerializable(NOTE_EXTRA_KEY);
        fillNote(note);

        buttonSave.setOnClickListener(v -> {
            getContract().saveNote(gatherNote());
        });
    }

    private void fillNote(Notes note) {
        if (note == null) return;
        editTextTitle.setText(note.subject);
        editTextDescription.setText(note.description);
    }


    private Notes gatherNote() {
        return new Notes(
                note == null ? Notes.newId() : note.id,
                editTextTitle.getText().toString(),
                editTextDescription.getText().toString(),
                note == null ? Notes.getCurrentDate() : note.creationDate
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof Contract)) {
            throw new IllegalStateException("Activity must implements Contract");
        }
    }

    private Contract getContract() {
        return (Contract) getActivity();
    }

    interface Contract {
        void saveNote(Notes notes);
    }
}
