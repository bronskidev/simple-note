package ru.gb.simplenotes;

import android.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class NoteViewHolder extends RecyclerView.ViewHolder {

    private final TextView subjectTextView;
    private final TextView descriptionTextView;
    private final CardView cardView;
    private Notes note;

    public NoteViewHolder(@NonNull ViewGroup parent, @Nullable NotesAdapter.OnItemClickListener clickListener,
                          NotesAdapter.OnItemDeleteListener deleteListener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false));

        cardView = (CardView) itemView;
        subjectTextView = itemView.findViewById(R.id.title_text_view);
        descriptionTextView = itemView.findViewById(R.id.description_text_view);
        //cardView.setCardBackgroundColor(new Random().nextInt());
        cardView.setOnClickListener(v -> {
            if (clickListener != null) {
                clickListener.onItemClick(note);
            }
        });

        itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                contextMenu.setHeaderTitle(R.string.context_menu);

                contextMenu.add(R.string.delete_note).setOnMenuItemClickListener(item -> {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(R.string.alert_title)
                            .setMessage(R.string.alert_message)
                            .setCancelable(false)
                            .setPositiveButton(R.string.positive_button_yes, (d, i) -> {
                                deleteListener.onDeleteItem(note);
                                Toast.makeText(itemView.getContext(), R.string.positive_button_yes, Toast.LENGTH_SHORT).show();
                            })
                            .setNegativeButton(R.string.negative_button_no, (d, i) -> {
                                Toast.makeText(itemView.getContext(), R.string.negative_button_no, Toast.LENGTH_SHORT).show();
                            })
                            .show();
                    Toast.makeText(itemView.getContext(), R.string.delete_note, Toast.LENGTH_SHORT).show();
                    return true;
                });
            }
        });
    }


    public void bind(Notes note) {
        this.note = note;
        subjectTextView.setText(note.subject);
        descriptionTextView.setText(note.description);
    }
}

