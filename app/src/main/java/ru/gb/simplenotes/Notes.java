package ru.gb.simplenotes;

import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

public class Notes implements Serializable {

    public final String id;
    public final String subject;
    public final String description;
    public final long creationDate;

    public Notes(String id, String subject, String description, long creationDate) {
        this.id = id;
        this.subject = subject;
        this.description = description;
        this.creationDate = creationDate;
    }

    public static String newId() {
        return UUID.randomUUID().toString();
    }

    public static long getCurrentDate() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
